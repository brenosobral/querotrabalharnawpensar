<?php
// DIC configuration
$container = $app->getContainer();

// Database
$container['pdo'] = function ($c) {
    $dsn = 'mysql:host='.$c['settings']['db']['host'].';dbname='.$c['settings']['db']['database'].';charset=utf8';
    $usr = $c['settings']['db']['username'];
    $pwd = $c['settings']['db']['password'];
    $pdo = new PDO($dsn, $usr, $pwd);  
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['view'] = new \Slim\Views\PhpRenderer("templates/");

$container['fieldsArray'] = ['id', 'prod_nome', 'prod_quant', 'prod_valor'];

$container['App\ProdutosMapper'] = function ($c) {
    return new App\ProdutosMapper($c['view'], $c['router'], $c['pdo'], $c['fieldsArray']);
};

