<?php

// Routes configuration

$app->get('/', 'App\ProdutosMapper:getProdutosAll')->setName('list-produto');

$app->get('/produto/new', 'App\ProdutosMapper:getProduto')->setName('get-Produto');

$app->get('/produto/{id}', 'App\ProdutosMapper:getProdutoById')->setName('produto-detail');

$app->get('/produto/edit/{id}', 'App\ProdutosMapper:edit')->setName('produto-edit');

$app->get('/produto/delete/{id}', 'App\ProdutosMapper:delete')->setName('produto-delete');

$app->post('/produto/update/{id}', 'App\ProdutosMapper:update')->setName('produto-update');

$app->post('/produto/new', 'App\ProdutosMapper:save')->setName('add-produto');

