<?php
namespace App;

use PDO;

class ProdutosMapper
{

    private $view;
    private $router;
    protected $pdo;
    private $fieldsArray;

    // Class constructor with view, router, PDO object and column array
    public function __construct($view, $router, $pdo, $fieldsArray)
    {
        $this->view = $view;
        $this->router = $router;
        $this->pdo = $pdo;
        $this->fieldsArray = $fieldsArray;
    }

    public function getProdutosAll($request, $response) {

        $statement = $this->pdo->prepare('SELECT * FROM produto order by id ASC');
        $statement->execute();
        $results = [];
        while($row = $statement->fetch()) {
            $results[] = new ProdutosEntity($row);
        }

        $response = $this->view->render($response, "produtos.phtml", ["produto" => $results, "router" => $this->router]);
        return $response;
    }

    public function getProduto($request, $response) {

        $response = $this->view->render($response, "adicionar_produto.phtml");
        return $response;
    }

    public function getProdutoById($request, $response, $args) {

        $produto_id = (int)$args['id'];

        $produto_id = filter_var($produto_id, FILTER_SANITIZE_STRING);

        $statement = $this->pdo->prepare('SELECT id, prod_nome, prod_quant, prod_valor 
            FROM produto where id = :produto_id order by id ASC');
        $statement->bindParam("produto_id", $produto_id);
        $statement->execute();

        $produto = new ProdutosEntity($statement->fetch());

    $response = $this->view->render($response, "detalhe_produto.phtml", ["produto" => $produto, "router" => $this->router]);
    return $response;

}

    public function save($request, $response, $args) {

        $data = $request->getParsedBody();

        $sql = "INSERT into produto(prod_nome, prod_quant, prod_valor) values (:prod_nome, :prod_quant, :prod_valor)";

        $produtos_data = [];
        $fieldsInserted = array_shift($this->fieldsArray);

        foreach($this->fieldsArray as $fieldData)
        {
            $produtos_data[$fieldData] = filter_var($data[$fieldData], FILTER_SANITIZE_STRING);
        }

        $produtos = new ProdutosEntity($produtos_data);

          try {

            $statement = $this->pdo->prepare($sql);
            $statement->bindParam("prod_nome", $produtos->getNome());
            $statement->bindParam("prod_quant", $produtos->getQuant());
            $statement->bindParam("prod_valor", $produtos->getValor());
            $statement->execute();


        $response = $response->withRedirect("/");
        return $response;

          } catch(PDOException $e) {
              echo json_encode($e->getMessage());
          }
    }

    public function edit($request, $response, $args) {

        $data = $request->getParsedBody();

        $produto_id = (int)$args['id'];
        $produto_id = filter_var($produto_id, FILTER_SANITIZE_STRING);


        $statement = $this->pdo->prepare('SELECT id, prod_nome, prod_quant, prod_valor 
            FROM produto where id = :produto_id order by id ASC');
        $statement->bindParam("produto_id", $produto_id);
        $statement->execute();

        $produto = new ProdutosEntity($statement->fetch());
     
        $response = $this->view->render($response, "atualizar_produto.phtml", ["produto" => $produto]);
        return $response;
    }


    public function update($request, $response, $args) {

        $data = $request->getParsedBody();

        $sql = "UPDATE produto set prod_nome = :prod_nome, prod_quant = :prod_quant, prod_valor = :prod_valor where id = :id";

        $produto_data = [];
        $produto_data['id'] = $produto_id = (int)$args['id'];

        $fieldsInserted = array_shift($this->fieldsArray);

        foreach($this->fieldsArray as $fieldData)
        {
            $produto_data[$fieldData] = filter_var($data[$fieldData], FILTER_SANITIZE_STRING);
        }

        $produto = new ProdutosEntity($produto_data);

        $statement = $this->pdo->prepare($sql);
        $statement->bindParam("id", $produto->getId());
        $statement->bindParam("prod_nome", $produto->getNome());
        $statement->bindParam("prod_quant", $produto->getQuant());
        $statement->bindParam("prod_valor", $produto->getValor());

        $statement->execute();

        $response = $response->withRedirect("/");

        return $response;
    }

    public function delete($request, $response, $args) {

        $data = $request->getParsedBody();

        $produto_id = (int)$args['id'];
        
        $statement = $this->pdo->prepare('DELETE
            FROM produto where id = :produto_id');
        $statement->bindParam("produto_id", $produto_id);
        $statement->execute();

        $response = $response->withRedirect("/");
        return $response;
    }


}




