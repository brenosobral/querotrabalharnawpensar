<?php
namespace App;

class ProdutosEntity
{
    protected $id;
    protected $prod_nome;
    protected $prod_desc;
    protected $prod_quant;
    protected $prod_valor;

    public function __construct(array $data) {

        if(isset($data['id'])) {
            $this->id = $data['id'];
        }

        $this->prod_nome = $data['prod_nome'];
        $this->prod_quant = $data['prod_quant'];
        $this->prod_valor = $data['prod_valor'];
    }
    public function getId() {
        return $this->id;
    }
    public function getNome() {
        return $this->prod_nome;
    }
    public function getQuant() {
        return $this->prod_quant;
    }
    public function getValor() {
        return $this->prod_valor;
    }
}