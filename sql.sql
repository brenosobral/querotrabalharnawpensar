CREATE DATABASE SuperWP;

use SuperWP

CREATE TABLE IF NOT EXISTS produto (
 id INT NOT NULL AUTO_INCREMENT,
 prod_nome VARCHAR(30),
 prod_quant INT,
 prod_valor double,
 PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS compra (
 id_compra INT NOT NULL AUTO_INCREMENT,
 id_produto INT DEFAULT '0',
 compra_numero INT,
 data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (id_compra),
 FOREIGN KEY (id_produto) REFERENCES produto (id)
 ON DELETE CASCADE
 ON UPDATE CASCADE
);

INSERT into produto(prod_nome,prod_quant,prod_valor) values ("Mettalica CD", 1, 40);
INSERT into produto(prod_nome,prod_quant,prod_valor) values ("Bruno Mars CD", 1, 30);
INSERT into produto(prod_nome,prod_quant,prod_valor) values ("Livro C++", 1, 20);
INSERT into produto(prod_nome,prod_quant,prod_valor) values ("Livro PHP", 1, 10);




















