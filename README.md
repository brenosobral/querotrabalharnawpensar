# ** Porque trabalhar na WPensar? ** #

* Porque quero trabalhar num ambiente MERITOCRÁTICO. Aqui os melhores são reconhecidos sempre!

* Porque quero aprender coisas novas a cada minuto.

* Porque quero ajudar a construir a maior empresa de tecnologia para educação do Brasil.

* Porque tenho sede por novos desafios.

* Porque quero fazer diferença!

# ** Como faço para me candidatar? ** #

1. Faça um fork do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Mande um pull request com o curriculo e a resposta do desafio

## ** Caso você não deseje que o envio seja público ** ##

1. Faça um clone do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Envie um email com o curriculo e um arquivo patch para rh@wpensar.com.br

# **Desafio:** #

O conceito desse desafio é nos ajudar a avaliar as habilidades dos candidatos às vagas de backend.

Você tem que desenvolver um sistema de estoque para um supermercado.

Esse supermercado assume que sempre que ele compra uma nova leva de produtos, ele tem que calcular o preço médio de compra de cada produto para estipular um preço de venda.
Para fins de simplificação assuma que produtos que tenham nomes iguais, são o mesmo produto e que não existe nem retirada e nem venda de produtos no sistema.

O valor calculado de preço médio deve ser armazenado.

Seu sistema deve:

1. Cadastro de produtos (Nome)
2. Compra de produtos (Produto, quantidade e preço de compra)
3. Listagem dos produtos comprados separados por compra (Nome, quantidade, preço de compra, preço médio)
4. Ser fácil de configurar e rodar em ambiente Unix (Linux ou Mac OS X)
5. Ser WEB
6. Ser escrita em Python 3.4+
7. Só deve utilizar biliotecas livres e gratuitas

Esse sistema não precisa ter, mas será um plus:

1. Autenticação e autorização (se for com OAuth, melhor ainda)
2. Ter um design bonito
3. Testes automatizados
4. Usar Jinja Template Engine para fazer o Front End.
5. Disponibilizar o Projeto na Nuvem(Heroku, Aws, ou similar)

# **Avaliação:** #

Vamos avaliar seguindo os seguintes critérios:

1. Você conseguiu concluir os requisitos?
2. Você documentou a maneira de configurar o ambiente e rodar sua aplicação?

# **Condições e Benefícios Oferecidos:** #

* Local: No meio de Icarai, Niterói - RJ
* Regime CLT
* Horário Flexível
* Gestão Horizontal
* Vale Refeição
* Vale Transporte
* Plano de Saúde (CLT)
* Ambiente descontraído
* Grande possibilidade de crescimento
* Trabalhar com projetos que mudam a Educação no País
* Sinuca

## Configurações:

Execute o shell ./init.sh faça o build em 3 sem cache e depois inicie os containers em 1.


```
https://bitbucket.org/brenosobral/querotrabalharnawpensar/src/master/ 
 
 
 _            _     __        ______                           
| |_ ___  ___| |_ __\ \      / /  _ \ ___ _ __  ___  __ _ _ __ 
| __/ _ \/ __| __/ _ \ \ /\ / /| |_) / _ \ '_ \/ __|/ _` | '__|
| ||  __/\__ \ ||  __/\ V  V / |  __/  __/ | | \__ \ (_| | |   
 \__\___||___/\__\___| \_/\_/  |_|   \___|_| |_|___/\__,_|_|   
                                                               
php7, nginx, mysql 
 
DOCKER
Generate new containers ? [ 1 ] 
Delete all containers ?   [ 2 ] 
Start new build ?         [ 3 ]

```

Apos os containers sendo executados com sucesso devera ficar assim.
```
           Name                         Command               State                 Ports              
-------------------------------------------------------------------------------------------------------
 CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                                      NAMES
6b57e7d8638e        digitalocean.com/php   "docker-php-entrypoi…"   12 minutes ago      Up 12 minutes       9000/tcp                                   app
ba888570605e        nginx:alpine           "nginx -g 'daemon of…"   22 minutes ago      Up 12 minutes       0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   webserver
8978d88da8bc        mysql:5.7.22           "docker-entrypoint.s…"   22 minutes ago      Up 12 minutes       0.0.0.0:3306->3306/tcp                     db

```

Caso apareça, e que esta tudo correto.

## Banco de Dados:

Ao subir os containers veja se o MYSQL esta rodando normalmente, execute para pegar o IP para acessar no banco. 
```
docker inspect db
```
Va ao final do terminal você vera, copie o Gateway para ```app/settings.php``` inserindo o IP no host.

```
                    "NetworkID": "74d3372dc8dd8b22b86b5b6b5a269a6ee1c5e01d1d68f313dd8203ebb81304ab",
                    "EndpointID": "196e5c8bcfc6bfe6d0af61784b3cf445cb69b0ef24ada4b1cc04e2001d17d1f9",
                    "Gateway": "172.20.0.1",
                    "IPAddress": "172.20.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:14:00:02",
                    "DriverOpts": null
```
Ira ficar assim.
```
 'host'      => '172.21.0.1:3306',
```
Apos isto vamos popular o nosso banco de dados, logando no MySQL.
```
docker exec -it db bash
```
Dentro do container, execute.
``` 
mysql -uroot -proot
```
Apos isso, rode o script sql.sql

## Testes

Va em http://0.0.0.0 e teste a aplicação.